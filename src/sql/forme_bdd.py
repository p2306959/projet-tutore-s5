import pandas as pd
import numpy as np

def film1():
    df2 = pd.read_table("dataFilm.tsv", index_col='tconst')
    dfFilm = df2.loc[df2['titleType']=='movie']
    dfFilm.to_csv("rawMovie.csv")
    dfFilm = dfFilm.drop(['titleType', 'originalTitle', 'endYear', 'genres'], axis=1)
    return dfFilm

def film2():
    dfFilm = film1()
    dfRate = pd.read_table("dataRating.tsv", index_col='tconst')
    dfFilm = dfFilm.join(dfRate)
    nul = dfFilm.loc['tt9916706', "runtimeMinutes"]
    dfFilm = dfFilm.replace([nul], np.nan)
    return dfFilm

def film3():
    film2()
    dfAkas = pd.read_table("dataAkas.tsv", index_col='titleId')
    dfAkas = dfAkas.replace(['\\N'], np.nan)
    dfAkas = dfAkas.loc[dfAkas['region'] == 'FR']
    dfAkas = dfAkas.drop(columns=['ordering', 'region', 'language', 'types', 'attributes', 'isOriginalTitle'])
    dfAkas = dfAkas.rename(columns={'title':'titleFR'})
    dfFilm = pd.read_csv("Film2.csv", index_col='tconst')
    dfFilm = dfFilm.join(dfAkas.groupby('titleId').agg({'titleFR': ' / '.join}))
    dfFilm.to_csv('Film.csv')

def genre1():
    df2 = pd.read_table("dataFilm.tsv", index_col='tconst')
    dfFilm = df2.loc[df2['titleType']=='movie']
    allGenre = dfFilm.loc[:,"genres"]
    tab = []
    for i in list(allGenre):
        tab += str(i).split(',')
    dfGenre = pd.DataFrame(tab)
    dfGenre = dfGenre.replace(['\\N'], np.nan)
    dfGenre = dfGenre.dropna()
    dfGenre = dfGenre.rename(columns={0 : 'nom'})
    dfGenre = dfGenre.nom.drop_duplicates()
    dfGenre.index = range(len(dfGenre.index))
    dfGenre.to_csv("Genre.csv")

def perso1():
    df = pd.read_table("dataPrincipals.tsv", index_col='tconst')
    dfFilm = pd.read_csv("Film.csv", index_col='tconst')
    df2 = df[df.index.isin(dfFilm.index)]
    df2 = df2.drop(columns=['job'])
    return df2

def perso2(): 
    df2 = perso1()
    dfPerso = pd.read_table("dataPersonne.tsv", index_col='nconst')
    dfPerso = dfPerso[dfPerso.index.isin(df2['nconst'])]
    dfPerso = dfPerso.drop(columns=['knownForTitles'])
    dfPerso = dfPerso.replace(['\\N'], np.nan)
    dfPerso[['prenom', 'nom']]= dfPerso["primaryName"].str.split(" ", n=1, expand=True)
    dfPerso = dfPerso.drop(columns=['primaryName'])
    dfPerso.to_csv("Personne.csv")

def role():
    df2 = perso1()
    df2 = df2.drop(columns=['ordering'])
    df2 = df2.replace(['\\N'], np.nan)
    df2.to_csv("Role.csv")

def genreFilm1():
    dfFilm = pd.read_csv("rawMovie.csv", index_col='tconst')
    dfGenre = pd.read_csv("Genre.csv", index_col=0)
    dfgenreFilm = pd.DataFrame(index=[], columns=["genre"])
    listGenre = dfGenre['nom'].to_numpy()
    cpt = 0
    for ind in dfFilm.index:
        for genre in listGenre :
            if genre in dfFilm['genres'][ind] :
                dfgenreFilm = pd.concat([dfgenreFilm, pd.DataFrame({'genre' : np.where(listGenre == genre)[0]}, index = [ind])])

    dfgenreFilm.to_csv("GenreFilm.csv")

def genreFilm3():
    dfFilm = pd.read_csv("rawMovie.csv", index_col='tconst')
    dfGenre = pd.read_csv("Genre.csv", index_col=0)

    genre_id_map = {genre: genre_id for genre_id, genre in enumerate(dfGenre['nom'])}
    genres_set = set(dfGenre['nom'])
    dfFilm['genre_ids'] = dfFilm['genres'].apply(lambda x: [genre_id_map[g] for g in genres_set if g in x])
    dfFilm = dfFilm[dfFilm['genre_ids'].apply(len) > 0]
    dfgenreFilm = pd.DataFrame({'genre_id': dfFilm['genre_ids'].explode()})
    
    dfgenreFilm.to_csv("GenreFilm.csv")

genreFilm3()
