const express = require('express');
const router = express.Router();
const controller = require('../controllers/userController');
const recommandationController = require('../controllers/recommendation');
const auth = require('../middlewares/auth');

router.get('/',auth.auth,controller.getUserInformations);

router.post('/login',controller.login);

router.post('/signup', controller.signup);

router.get('/logout',auth.auth,controller.logout);

router.get('/seenMovies',auth.auth,controller.getUserSeenMovies);

// router.put('/',auth.auth,controller.changeUserInfos);

router.get('/recommandation',auth.auth,recommandationController.getRecommandations);


module.exports = router;