const express = require('express');
const router = express.Router();
const controller = require('../controllers/movieController');
const auth = require('../middlewares/auth');

router.get('/:id(tt\\d+)',controller.getMovieInformations);
router.get('/search',controller.searchMovie);
router.get('/criteriasearch',controller.searchMovieByCriteria);
// router.get('/recent',controller.getRecentlyReleasedMovies);
router.get('/cast/:id(tt\\d+)',controller.getMovieCast);
router.get('/addmovienote',auth.auth,controller.addMovieNote);
router.get('/getMovieNote',auth.auth,controller.getMovieNote);
router.delete('/:id(tt\\d+)',controller.deleteMovieNote);
router.get('/getAllStyles',controller.getAllStyles);
router.get('/styles/:id(tt\\d+)',controller.getMovieStyles);

router.get('/topmovies',controller.getTopMovies);


module.exports = router;