const express = require('express');
const router = express.Router();
const controller = require('../controllers/personController');

router.get('/:id(nm\\d+)',controller.getPersonInformations);
router.get('/movies/:id(nm\\d+)',controller.getPersonMovies);
router.get('/search',controller.searchPerson);



module.exports = router;