const movieServices = require('../services/movies')
const db = require('../models/db');


exports.getMovieInformations = async (req, res, next) => {
    const filmId = req.params.id;

    try {
        let result = await movieServices.getMovieById(filmId);
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({ error: error.toString() });
    }
};

exports.searchMovie = async (req, res, next) => {
    let search = req.query.title;

    try {
        let result = await movieServices.searchMovie(search);
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({ error: error.toString() });
    }
};

exports.searchMovieByCriteria = async (req, res, next) => {
    let title = req.query.title;
    let dateMin = req.query.dateMin;
    let dateMax = req.query.dateMax;
    let genres = req.query.genres || [];
    let personnes = req.query.personnes || [];
    let order = req.query.order;
    let croissant = req.query.croissant === 'true';

    try {
        let result = await movieServices.searchMovieByCriteria(title,dateMin,dateMax,genres,personnes,order,croissant);
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({ error: error.toString() });
    }
};


// exports.getRecentlyReleasedMovies = (req, res, next) => {
//     const numberOfMovies = req.query.numberOfMovies || 20;

//     db.query('SELECT * FROM Film WHERE releaseDate <= CURDATE() ORDER BY releaseDate DESC LIMIT ?',
//         [parseInt(numberOfMovies)], (error, results, fields) => {
//             if (error) {
//                 res.status(500).json({ error : error.toString()});
//             } else {
//                 res.status(200).json(results);
//             }
//         });
// };

exports.getMovieCast = async (req, res, next) => {
    let filmId = req.params.id;

    try {
        let result = await movieServices.getMovieCast(filmId);
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({ error: error.toString() });
    }
};

exports.getMovieNote = async (req, res, next) => {
    let filmId = req.query.id;
    let username = req.session.username;

    try {
        let result = await movieServices.getMovieUserRate(filmId, username);
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({ error: error.toString() });
    }

};

exports.addMovieNote = async (req, res, next) => {
    let filmId = req.query.id;
    let username = req.session.username;
    let note = req.query.note;

    try {
        let result = await movieServices.insertOrUpdateMovieUserRate(filmId, username, note);
        res.status(200).json({ message: 'Note ajoutée ou modifiée' });
    } catch (error) {
        res.status(500).json({ error: error.toString() });
    }

};

exports.deleteMovieNote = async (req, res, next) => {
    let filmId = req.params.id;
    let username = req.session.username;

    try {
        let result = await movieServices.deleteMovieUserRate(filmId, username);
        res.status(200).json({ message: 'Note supprimée' });
    } catch (error) {
        res.status(500).json({ error: error.toString() });
    }

};

exports.getAllStyles = async (req, res, next) => {
    try {
        let result = await movieServices.getAllStyles();
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({ error: error.toString() });
    }
};


exports.getMovieStyles = async (req, res, next) => {
    let filmId = req.params.id;

    try {
        let result = await movieServices.getMovieStyles(filmId)
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({ error: error.toString() });
    }
};


exports.getTopMovies = (req, res, next) => {
    const numberOfMovies=64;

    console.log(`Fetching top ${numberOfMovies} movies`);

    db.query('SELECT * FROM Film ORDER BY averageRating DESC LIMIT ?', [parseInt(numberOfMovies)], (error, results, fields) => {
        if (error) {
            console.error('Error executing SQL query:', error);
            res.status(500).json({ error });
        } else {
            console.log('SQL query executed successfully.');
            console.log('Number of movies fetched:', results.length);
            res.status(200).json(results);
        }
    })
};
