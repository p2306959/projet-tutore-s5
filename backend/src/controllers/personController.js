const db = require('../models/db');
const personServices = require('../services/person');

exports.getPersonInformations = async (req, res, next) => {
    let personId = req.params.id;

    try {
        let result = await personServices.getPersonById(personId);
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({ error : error.toString()});
    }
};
exports.getPersonMovies = async (req, res, next) => {
    let personId = req.params.id;

    try {
        let result = await personServices.getPersonMovies(personId);
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({ error : error.toString()});
    }
};

exports.searchPerson = async (req, res, next) => {
    let searchedName = req.query.name;

    try {
        let result = await personServices.searchPerson(searchedName);
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({ error : error.toString()});
    }
};