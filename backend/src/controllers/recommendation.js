
const Movie = require("../models/class_film.js");
const User = require("../models/class_user.js");
const { getUserInformations, getUserSeenMovies } = require("../services/user.js");
const { getMovieAuthor, getMovieActors, getMovieStyles, getMovieById, getFilmsByAAS, getMovieCast } = require("../services/movies.js");
const controllers = require("./userController.js");
const { getSeenMovieAll, top3Authors, top3Actors, top3Styles, searchMovie } = require("../models/movieDb.js");

exports.getRecommandations = async (req, res, next) => {
    let username = req.session.username;
    res.status(200).json(await recommendationForUser(username));
};

/**
 * Cette fonction selectionne les 3 meilleurs films pour un utilisateur.
 *
 * @param {string} user_login - Login de l'utilisateur.
 * @returns {Movie[]} - La liste des 3 films.
 */

async function recommendationForUser(user_login, max_number = 3) {
    let fav_authors = await top3Authors(user_login);
    let fav_actors = await top3Actors(user_login);
    let fav_styles = await top3Styles(user_login);
    
    let personnes = [];
    let genres = [];
    let points_films = [[]];
    personnes = personnes.concat(fav_authors.map(author => author['nconst']));
    personnes = personnes.concat(fav_actors.map(actor => actor['nconst']));
    genres = genres.concat(fav_styles.map(style => style['idGen']));

    let all_films = await getFilmsByAAS(personnes, genres, 'averageRating', false, user_login);
    
    let resolvedAllFilms = await Promise.all(all_films.map(film => getMovieEveryInfos(film['tconst'])));
    let max_point_author = fav_authors.length * fav_authors.length;
    let max_point_actor = fav_actors.length * fav_actors.length;
    let max_point_style = fav_styles.length * fav_styles.length;
    for (let i = 0; i < resolvedAllFilms.length; i++) {
        points_films[i] = [];
        points_films[i][0] = resolvedAllFilms[i];
        points_films[i][1] = 0;
        points_films[i][2] = 0;
        points_films[i][3] = 0;
        if (resolvedAllFilms[i].author != undefined) {
            for (let j = 0; j < fav_authors.length; j++) {
                if (resolvedAllFilms[i].author == fav_authors[j]){
                    points_films[i][1] = (fav_authors.length - j) * fav_authors.length;
                }
            }
        }
        if (resolvedAllFilms[i].actors != undefined) {
            for (let j_1 = 0; j_1 < fav_actors.length; j_1++) {
                resolvedAllFilms[i].actors.forEach(actor => {
                    if (actor == fav_actors[j_1]) {
                        points_films[i][2] += (fav_actors.length - j_1) * fav_actors.length / resolvedAllFilms[i].actors.length;
                    }
                });
            }
        }else{
            console.log(resolvedAllFilms[i].actors);
        }
        if (resolvedAllFilms[i].styles != undefined) {
            for (let j_2 = 0; j_2 < fav_styles.length; j_2++) {
                resolvedAllFilms[i].styles.forEach(style => {
                    if (style == fav_styles[j_2].idGen) {
                        points_films[i][3] += (fav_styles.length - j_2) * fav_styles.length / resolvedAllFilms[i].styles.length;
                    }
                });
            }
        }
        points_films[i][4] = points_films[i][1] / max_point_author + points_films[i][2] / max_point_actor + points_films[i][3] / max_point_style;
        points_films[i][4] /= 3;
    }

    let res = [];
    for (let i_1 = 0; i_1 < max_number; i_1++) {
        res.push(getHighestScore(points_films, res));
    }
    for (let i = 0; i < res.length; i++) {
        res[i] = await getMovieById(res[i].id);
    }
    return res;
}

/**
 * Renvoie les informations du film avec ses acteurs, auteur et styles en classe Movie
 *
 * @param {string} movieId - L'id du film (tconst)
 * @returns {Promise<Movie>} - Le film en classe Movie avec l'auteur, les acteurs et les styles
 */
async function getMovieEveryInfos(movieId) {
    let movie = await getMovieById(movieId);
    //console.log("gMEI : movie : " + movie + " movieId:" + movieId);
    movie = Movie.toMovie(movie); // Suppose que Movie.toMovie retourne directement l'objet modifié
    //console.log(movie);
    movie.author = (await getMovieCast(movieId))[0];

    //console.log("gMEI : movie.author : " + movie.author + " movieId:" + movieId);
    //console.log(movie.author);
    movie.author = movie.author?.nconst; // Assurez-vous que nconst est défini après la résolution
    //console.log(movie.author);
    let resolvedActors = (await getMovieCast(movieId)).slice(1);
    movie.actors = resolvedActors.map((actor) => actor?.nconst);

    let resolvedStyles = await getMovieStyles(movieId);
    movie.styles = resolvedStyles.map(style => style?.idGen);

    return movie;
}



function getHighestScore(points_films, blacklist) {
    let found = false;
    let i = 0;
    let res;
    let highscore = -1;
    while (i < points_films.length && !found) {
        if (points_films[i][4] > highscore) {
            if (!isIn(points_films[i][0], blacklist)) {
                highscore = points_films[i][4];
                res = points_films[i][0];
            }
        }
        i++;
    }
    return res;
}

/**
 * Vérifie qu'un object est dans une liste
 *
 * @param {Object} objet - objet a trouver
 * @param {Object[]} liste - liste dans laquelle chercher
 * 
 * @returns {boolean} - true Si l'objet est dans la liste
 */
function isIn(objet, liste) {
    let res = false;
    let i = 0;
    while (res == false && i < liste.length) if (objet == liste[i++]) res = true;
    return res;
}
