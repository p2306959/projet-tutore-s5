const bcrypt = require('bcryptjs');
const db = require('../models/db');
const moovie = require("../models/class_film.js")
const User = require("../models/class_user.js")
const userServices = require('../services/user');


exports.signup = async (req, res, next) => {

    let { username, lastname, firstname, birthdate, password } = req.body;

    try {
        let result = await userServices.signup(username, lastname, firstname, birthdate, password)
        res.status(201).json({ message: 'Utilisateur ajouté' });
    } catch (error) {
        res.status(500).json({ error : error.toString()});
    }

};

exports.login = async (req, res, next) => {

    let { username, password } = req.body;

    try {
        let loggedin = await userServices.login(username, password);
        if (loggedin) {
            req.session.username = username;
            res.status(200).json({ message: 'Connecté !' });
        } else {
            res.status(401).json({ message: 'Identifiant ou mot de passe incorrect' });
        }

    } catch (error) {
        res.status(500).json({ error : error.toString()});
    }

};

exports.logout = (req, res, next) => {
    req.session.username = null;
    res.status(200).json({ message: 'Déconnecté !' });
};


exports.getUserInformations = async (req, res, next) => {
    let username = req.session.username;
    //renvoyer login, nom, prenom, dateNais


    try {
        let result = await userServices.getUserInformations(username);
        let infos = {
            username: result.login,
            lastname: result.nom,
            firstname: result.prenom,
            birthdate: result.dateNais
        };
        res.status(200).json(infos);
    } catch (error) {
        res.status(500).json({ error : error.toString()});
    }

};

exports.getUserSeenMovies= async (req, res, next) => {
    let username = req.session.username;

    try {
        let result = await userServices.getUserSeenMovies(username);
        res.status(200).json(result);
    } catch (error) {
        res.status(500).json({ error : error.toString()});
    }
};





/**
 * Cette fonction Récupere les films d'un auteur.
 *
 * @param {string} author - L'auteur cherché.
 * @param {boolean} isOrdered - Faut il trier la liste.
 * @param {int} length - La taille de la liste (-1 = max).
 * @returns {Movie[]} - La liste des films en type Film.
 */

exports.getMoviesByAuthor = (author, isOrdered = false, length = -1) =>{
    res = db.query('Select f.tconst, f.primaryTitle, f.isAdult, f.startYear, f.runtimeMinutes, f.averageRating, f.numVotes, f.titleFR From Film f join Role r on f.tconst = r.tconst  where r.nconst = ?', [author]);
    return Movie.toMovie(res);
}

/**
 * Cette fonction Récupere les films d'un Style.
 *
 * @param {int} style - Le style cherché.
 * @param {boolean} isOrdered - Faut il trier la liste.
 * @param {int} length - La taille de la liste (-1 = max).
 * @returns {Movie[]} - La liste des films en type Film.
 */

exports.getMoviesByStyle = (style, isOrdered = false, length = -1) => {
    res = db.query('Select f.tconst, f.primaryTitle, f.isAdult, f.startYear, f.runtimeMinutes, f.averageRating, f.numVotes, f.titleFR From Film f joingenreFilm gf on f.tconst = gf.tconst  where gf.idgen = ?', [style]);
    return Movie.toMovie(res);
}



/**
 * Cette fonction récupere un utilisateur avec son login.
 *
 * @param {string} login - login de l'utilisateur.
 * @param {?} res - resultat ?
 * @returns {User} - L'utilisateur.
 */

exports.get_user = (req, res, next) =>{
    const queryResult = new Promise((resolve, reject) => {
        db.query('Select distinct * FROM Utilisateur WHERE login=?', ['sachatte'], (error, results, fields) => {
            if (error) {
                reject(error);
            } else {
                let infos = User.toUser(results[0])
                resolve(infos);
            }
        }
        
        );
    });
}

exports.changeUserPassword = async (req,res,next) => {
    let username = req.session.username;
    let newPassword = req.body.newPassword;
    try {
        await userServices.changeUserPassword(username,newPassword)
        res.status(200).json({ message: 'Mot de passe modifié' });
    } catch (error) {
        res.status(500).json({ error : error.toString()});
    }
}
