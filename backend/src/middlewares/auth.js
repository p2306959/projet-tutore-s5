const db = require('../models/db');

exports.auth = (req, res, next) => {
    if (req.session.username) {
        next();
    } else {
        res.status(401).json({message : 'Non authentifié'});
    }
}

