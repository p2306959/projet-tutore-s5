const db = require('../models/db');
const { getUserSeenMovies } = require('../services/user');

/**
 * Récupère les informations d'un film à partir de son identifiant unique (tconst).
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @returns {Promise<Object>} Une promesse qui se résout avec un objet contenant les informations du film, ou se rejette avec une erreur.
 */
exports.getMovieById = (movieId) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM Film WHERE tconst = ?',
            [movieId], function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(results[0]);
                }
            })
    })
};

/**
 * Recherche des films par titre, en prenant en compte le titre principal et le titre en français.
 *
 * @param {string} searchTitle - Le titre à rechercher.
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations des films correspondant à la recherche.
 *                                  Chaque objet du tableau correspond à un film et a une structure similaire à celui renvoyé par getMovieById.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.searchMovieByTitle = (searchTitle) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM Film WHERE primaryTitle LIKE CONCAT("%",?,"%") OR titleFR LIKE CONCAT("%",?,"%") LIMIT 200',
            [searchTitle, searchTitle], function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results);
                }
            })
    })
};

/**
 * Recherche des films dans la base de données en fonction des critères spécifiés.
 *
 * @param {string} title - Le titre à rechercher (peut être null ou undefined).
 * @param {number} dateMin - La date minimale de sortie des films (peut être null ou undefined).
 * @param {number} dateMax - La date maximale de sortie des films (peut être null ou undefined).
 * @param {string[]} genres - Un tableau d'identifiants de genres pour filtrer les films (peut être vide).
 * @param {string[]} personnes - Un tableau d'identifiants de personnes pour filtrer les films (peut être vide).
 * @param {string} order - Le champ selon lequel les résultats doivent être triés (peut être null ou undefined).
 * @param {boolean} croissant - Indique si les résultats doivent être triés par ordre croissant (true) ou décroissant (false).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les films filtrés.
 *                                  La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.searchMovie = (title, dateMin, dateMax, genres, personnes, order, croissant) => {
    return new Promise((resolve, reject) => {
        let query = 'SELECT * FROM Film WHERE 1=1';
        let params = []

        if (title) {
            query += ' AND ( primaryTitle LIKE CONCAT("%", ?, "%") OR titleFR LIKE CONCAT("%", ?, "%") ) ';
            params = params.concat([title, title]);
        }

        if (dateMin) {
            query += ' AND startYear >= ?';
            params = params.concat([dateMin]);
        }

        if (dateMax) {
            query += ' AND startYear <= ?';
            params = params.concat([dateMax]);
        }

        if (genres.length) {
            query += ' AND EXISTS (SELECT 1 FROM genreFilm gf WHERE gf.tconst = Film.tconst AND gf.idGen IN (?) )';
            params = params.concat([genres]);
        }
        if (personnes.length) {
            query += ' AND EXISTS (SELECT 1 FROM role r WHERE r.tconst = Film.tconst AND r.nconst IN (?) )';
            params = params.concat([personnes]);
        }


        if (order) {
            query += ' ORDER BY ??';
            params = params.concat([order]);

            if (!croissant) {
                query += ' DESC';
            }
        }

        query += ' LIMIT 200';

        db.query(query, params, function (error, results, fields) {
            if (error) {
                return reject(error);
            } else {
                return resolve(results);
            }
        })
    })
};


/**
 * Récupère la distribution d'un film en fonction de son identifiant unique (tconst).
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les rôles et les personnes associées au film.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getMovieCast = (movieId) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM role r JOIN personne p on r.nconst=p.nconst WHERE r.tconst = ?',
            [movieId], function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(results);
                }
            })
    })
};


/**
 * Récupère l'auteur d'un film en fonction de son identifiant unique (tconst).
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @returns {Promise<Object>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les rôles et les personnes associées au film.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getMovieAuthor = (movieId) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM role r JOIN personne p on r.nconst = p.nconst WHERE r.tconst = "?" AND r.roles = "writer"',[movieId], function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(results[0]);
                }
            })
    })
};


/**
 * Récupère les acteurs d'un film en fonction de son identifiant unique (tconst).
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les rôles et les personnes associées au film.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getMovieActors = (movieId) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM role r JOIN personne p on r.nconst=p.nconst WHERE r.tconst = "?" AND (r.roles = "actor" OR r.roles = "actress")',
            [movieId], function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(results);
                }
            })
    })
};

exports.getSeenMovieAll = (userId) => {
    return new Promise((resolve, reject) => {
        db.query("Select u.login, f.tconst, r.roles, p.nconst, g.idGen, fv.note, f.averageRating, f.runtimeMinutes from utilisateur u join filmvue fv on u.login = fv.login join film f on fv.tconst = f.tconst join role r on f.tconst = r.tconst join personne p on r.nconst = p.nconst join genrefilm gf on f.tconst = gf.tconst join genre g on gf.idGen = g.idGen Where u.login = 'sachatte' Order by f.tconst, r.roles",
            [userId], function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(results);
                }
            })
    })
}



/**
 * Récupère les films associés à un genre particulier en fonction de l'identifiant unique du genre (idGen).
 *
 * @param {number} styleId - L'identifiant unique du genre (idGen).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les films associés au genre spécifié.
 *                                  Chaque objet du tableau a une structure similaire à celui renvoyé par getMovieById.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getMoviesByStyle = (styleId) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM genrefilm gf JOIN film f on gf.tconst = f.tconst WHERE f.idGen = ?', [styleId], function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(results);
                }
            })
    })
};


/**
 * Récupère les genres associés à un film en fonction de son identifiant unique (tconst).
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les genres associés au film.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getMovieStyles = (movieId) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM genrefilm gf JOIN genre g on gf.idGen = g.idGen WHERE gf.tconst = ?',
            [movieId], function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(results);
                }
            })
    })
};

/**
 * Récupère la note attribuée par un utilisateur à un film spécifique.
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @param {string} userId - L'identifiant unique de l'utilisateur (login).
 * @returns {Promise<Object|null>} Une promesse qui se résout avec un objet contenant les informations sur la note attribuée par l'utilisateur au film.
 *                                La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getMovieUserRate = (movieId, userId) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM Filmvue WHERE login=? and tconst=?', [userId, movieId],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results[0]);
                }
            })
    })
};

/**
 * Insère une nouvelle note attribuée par un utilisateur à un film spécifique.
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @param {string} userId - L'identifiant unique de l'utilisateur (login).
 * @param {number} rate - La note attribuée par l'utilisateur au film.
 * @returns {Promise<Object>} Une promesse qui se résout avec un objet contenant des informations sur l'opération d'insertion/mise à jour.
 *                           La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.insertMovieUserRate = (movieId, userId, rate) => {
    return new Promise((resolve, reject) => {
        db.query('INSERT INTO Filmvue VALUES (?,?,?)',
            [userId, movieId, rate],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results);
                }
            })
    })
};

/**
 * Met à jour la note attribuée par un utilisateur à un film spécifique dans la table "FilmVue".
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @param {string} userId - L'identifiant unique de l'utilisateur (login).
 * @param {number} rate - La nouvelle note attribuée par l'utilisateur au film.
 * @returns {Promise<Object>} Une promesse qui se résout avec un objet contenant des informations sur l'opération de mise à jour.
 *                           La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.updateMovieUserRate = (movieId, userId, rate) => {
    return new Promise((resolve, reject) => {
        db.query('UPDATE Filmvue SET note=? WHERE login=? and tconst=?', [rate, userId, movieId],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results);
                }
            })
    })
};

/**
 * Supprime la note attribuée par un utilisateur à un film spécifique dans la table "FilmVue".
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @param {string} userId - L'identifiant unique de l'utilisateur (login).
 * @returns {Promise<Object>} Une promesse qui se résout avec un objet contenant des informations sur l'opération de suppression.
 *                           La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.deleteMovieUserRate = (movieId, userId) => {
    return new Promise((resolve, reject) => {
        db.query('DELETE FROM Filmvue WHERE login=? and tconst=?', [userId, movieId],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results);
                }
            })
    })
};

exports.getFilmsByAAS = async (personnes, genres, order, croissant, userId) => {
    let seenMovies = (await getUserSeenMovies(userId)).map(movie => movie.tconst);

    return new Promise((resolve, reject) => {
        let query = 'SELECT * FROM Film WHERE (true';
        let params = []

        if (genres.length) {
            query += ' OR EXISTS (SELECT 1 FROM genreFilm gf WHERE gf.tconst = Film.tconst AND gf.idGen IN (?) )';
            params = params.concat([genres]);
        }
        if (personnes.length) {
            query += ' OR EXISTS (SELECT 1 FROM role r WHERE r.tconst = Film.tconst AND r.nconst IN (?) )';
            params = params.concat([personnes]);
        }
        query += ') AND tconst NOT IN (?)';
        params = params.concat([seenMovies]);

        if (order) {
            query += ' ORDER BY Rand()*10 and ??';
            params = params.concat([order]);

            if (!croissant) {
                query += ' DESC';
            }
        }
        query += ' LIMIT 2000';
        db.query(query, params, function (error, results, fields) {
            if (error) {
                return reject(error);
            } else {
                return resolve(results);
            }
        })
    })
}

exports.top3Actors = (userId) => {
    return new Promise((resolve, reject) => {
        db.query(`WITH TopActors AS (
            SELECT
                p.nconst,
                COUNT(DISTINCT fv.tconst) AS nombreDeFilmsEnTantQueActor,
                ROW_NUMBER() OVER (ORDER BY COUNT(DISTINCT fv.tconst) DESC) AS Classement
            FROM
                role r
            JOIN
                personne p ON r.nconst = p.nconst
            JOIN
                filmvue fv ON r.tconst = fv.tconst
            WHERE
                (r.roles = 'actor'
                OR r.roles = 'actress')
                AND fv.login = ?
            GROUP BY
                p.nconst
        )
        SELECT
            p.nconst,
            p.nom,
            ta.nombreDeFilmsEnTantQueActor
        FROM
            TopActors ta
        JOIN
            personne p ON ta.nconst = p.nconst
        WHERE
            ta.Classement <= 3
        ORDER BY
            ta.Classement;`, [userId],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results);
                }
            })
    })
}

exports.top3Authors = (userId) => {
    return new Promise((resolve, reject) => {
        db.query(`WITH TopWriters AS (
            SELECT
                p.nconst,
                COUNT(DISTINCT fv.tconst) AS nombreDeFilmsEnTantQueWriter,
                ROW_NUMBER() OVER (ORDER BY COUNT(DISTINCT fv.tconst) DESC) AS Classement
            FROM
                role r
            JOIN
                personne p ON r.nconst = p.nconst
            JOIN
                filmvue fv ON r.tconst = fv.tconst
            WHERE
                r.roles = 'writer'
                AND fv.login = ?
            GROUP BY
                p.nconst
        )
        SELECT
            p.nconst,
            p.nom,
            tw.nombreDeFilmsEnTantQueWriter
        FROM
            TopWriters tw
        JOIN
            personne p ON tw.nconst = p.nconst
        WHERE
            tw.Classement <= 3
        ORDER BY
            tw.Classement;`, [userId],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results);
                }
            })
    })
}

exports.top3Styles = (userId) => {
    return new Promise((resolve, reject) => {
        db.query(`WITH TopGenres AS (
            SELECT
                g.idGen,
                COUNT(DISTINCT fv.tconst) AS nombreDeFilmsDansCeGenre,
                ROW_NUMBER() OVER (ORDER BY COUNT(DISTINCT fv.tconst) DESC) AS Classement
            FROM
                genrefilm gf
            JOIN
                genre g ON gf.idGen = g.idGen
            JOIN
                filmvue fv ON gf.tconst = fv.tconst
            WHERE
                fv.login = ?
            GROUP BY
                g.idGen
        )
        SELECT
            g.idGen,
            g.nom,
            tg.nombreDeFilmsDansCeGenre
        FROM
            TopGenres tg
        JOIN
            genre g ON tg.idGen = g.idGen
        WHERE
            tg.Classement <= 3
        ORDER BY
            tg.Classement;`, [userId],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results);
                }
            })
    })
}

/**
 * Récupère tous les genres depuis la base de données.
 *
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur tous les genres.
 *                                  La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.getAllStyles = () => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM genre',
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results);
                }
            })
    })
};