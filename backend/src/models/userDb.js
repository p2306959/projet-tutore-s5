const db = require('../models/db');

/**
 * Insère un nouvel utilisateur dans la table "Utilisateur".
 *
 * @param {string} username - Le nom d'utilisateur du nouvel utilisateur.
 * @param {string} lastname - Le nom de famille du nouvel utilisateur.
 * @param {string} firstname - Le prénom du nouvel utilisateur.
 * @param {string} hash - Le hash du mot de passe du nouvel utilisateur.
 * @param {string} birthdate - La date de naissance du nouvel utilisateur (au format "YYYY-MM-DD").
 * @returns {Promise<Object>} Une promesse qui se résout avec un objet contenant des informations sur l'opération d'insertion.
 *                           La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.insertUser = (username, lastname, firstname, hash, birthdate) => {
    return new Promise((resolve, reject) => {
        db.query('INSERT INTO Utilisateur VALUES(?,?,?,?,?)',
            [username, lastname, firstname, hash, birthdate],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results);
                }
            })
    })
};

/**
 * Récupère les informations d'un utilisateur en fonction de son nom d'utilisateur.
 *
 * @param {string} username - Le nom d'utilisateur de l'utilisateur dont les informations sont récupérées.
 * @returns {Promise<Object|null>} Une promesse qui se résout avec un objet contenant les informations sur l'utilisateur.
 *                                La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getuserInformations = (username) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT login,nom,prenom,dateNais FROM Utilisateur WHERE login = ?',
            [username],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(results[0]);
                }
            })
    })
};

/**
 * Récupère le nom d'utilisateur et le hash du mot de passe d'un utilisateur en fonction de son nom d'utilisateur.
 *
 * @param {string} username - Le nom d'utilisateur de l'utilisateur dont le hash du mot de passe est récupéré.
 * @returns {Promise<Object|null>} Une promesse qui se résout avec un objet contenant le nom d'utilisateur et le hash du mot de passe.
 *                                La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getuserPasswordHash = (username) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT login, mdp FROM Utilisateur WHERE login=?',
            [username],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results[0]);
                }
            });
    })
};

/**
 * Récupère les films que l'utilisateur a déjà visionnés en fonction de son nom d'utilisateur.
 *
 * @param {string} username - Le nom d'utilisateur de l'utilisateur dont les films visionnés sont récupérés.
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations
 *                                   sur les films visionnés par l'utilisateur ainsi que la note qui leur est attribuée.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getUserSeenMovies = (username) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM filmvue fv JOIN film f on fv.tconst = f.tconst WHERE fv.login = ?',
            [username], function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(results);
                }
            })
    })
    
};


/**
 * Met à jour le mot de passe d'un utilisateur dans la base de données.
 *
 * @param {string} username - Le nom d'utilisateur de l'utilisateur dont le mot de passe doit être modifié.
 * @param {string} newPasswordHash - Le nouveau mot de passe hashé de l'utilisateur.
 * @returns {Promise<Object>} Une promesse qui se résout avec un objet contenant des informations sur l'opération de modification du mot de passe.
 *                           La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.changeUserPassword = (username, newPasswordHash) => {
    return new Promise((resolve, reject) => {
        db.query('UPDATE Utilisateur set mdp = ? WHERE login=?',
            [newPasswordHash, username],
            function (error, results, fields) {
                if (error) {
                    return reject(error);
                }
                else {
                    return resolve(results);
                }
            })
    })
}
