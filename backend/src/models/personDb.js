const db = require('../models/db');

/**
 * Récupère les informations sur une personne (acteur, réalisateur, etc.) en fonction de son identifiant unique.
 *
 * @param {string} personId - L'identifiant unique de la personne (nconst).
 * @returns {Promise<Object|null>} Une promesse qui se résout avec un objet contenant les informations sur la personne.
 *                                La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getPersonById = (personId) => {

    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM Personne WHERE nconst = ?',
            [personId], function (error, results, fields) {

                if (error) {
                    return reject(error);
                } else {
                    return resolve(results[0]);
                }
            })
    })
};

/**
 * Récupère les films associés à une personne (acteur, réalisateur, etc.) en fonction de son identifiant unique.
 *
 * @param {string} personId - L'identifiant unique de la personne (nconst).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les films associés à la personne.
 *                                  Chaque objet du tableau a une structure similaire à celui renvoyé par getPersonMovies.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getPersonMovies = (personId) => {

    return new Promise((resolve, reject) => {
        db.query('SELECT * FROM role r JOIN film f ON r.tconst=f.tconst WHERE r.nconst = ?',
            [personId], function (error, results, fields) {

                if (error) {
                    return reject(error);
                } else {
                    return resolve(results);
                }
            })
    })
};

/**
 * Recherche des personnes dans la base de données en fonction du nom ou du prénom.
 *
 * @param {string} searchedName - Le nom ou le prénom à rechercher.
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les personnes trouvées.
 *                                  Chaque objet contient le nom complet (concaténation du prénom et du nom) et l'identifiant unique de la personne (nconst).
 *                                  La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.searchPerson = (searchedName) => {
    return new Promise((resolve, reject) => {
        db.query('SELECT CONCAT(prenom," ",nom) as nom, nconst FROM personne WHERE CONCAT(prenom," ",nom) LIKE CONCAT("%",?,"%") OR CONCAT(nom," ",prenom) LIKE CONCAT("%",?,"%") LIMIT 20',
            [searchedName, searchedName], function (error, results, fields) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve(results);
                }
            })
    })
}