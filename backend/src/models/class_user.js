
module.exports = class User{ 
    constructor(login){
        this.login = login;
        this.movies = [];
    }

    addMovies(movieSeen){
        this.movies.push(movieSeen);
    }

    /**
    * Cette fonction convertit une requete SQL en User
    *
    * @param {[??]} param - Résultat requete.
    * @returns {User} - l'utilisateur
    */
    static toUser (param) {
        let login = param.login;
        return new User(login);
    }
}