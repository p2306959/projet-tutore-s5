
module.exports = class Movie{ 
    constructor(id, name, note){
        this.id = id;
        this.name = name;
        this.note = note;

        this.author = "";
        this.actors = [];
        this.styles = [];
    }

    addActors(actor){
        this.actors.push(actor);
    }

    addStyles(style){
        this.styles.push(style);
    }
    
    /**
    * Cette fonction convertit une requete SQL en Movie
    *
    * @param {[string, string, int, int, int, float, int, string] ??} param - Résultat requete.
    * @returns {Movie} - le film
    */
   
    static toMovie(param){
        return new Movie(param.tconst, param.primaryTitle, param.averageRating);
    }
}
