const express = require('express')
const app = express()
const port = 3000
var cors = require('cors');
var session = require('express-session');
const crypto = require('node:crypto');


app.use(express.json());
app.use(session({
  secret:crypto.randomUUID(),
  resave:false,
  saveUninitialized:false
}));
app.use(express.urlencoded({ extended: true }));
app.use(cors({origin: 'http://localhost:5173', credentials : true}));
const userRoutes = require('./routes/user');
app.use('/user', userRoutes);
const moviesRoutes = require('./routes/movies');
app.use('/movies',moviesRoutes);
const personRoutes = require('./routes/person');
app.use('/person',personRoutes);

app.listen(port, () => {
  console.log(`Serveur en cours d'exécution sur le port ${port}`);
})