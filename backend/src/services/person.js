const personDb = require('../models/personDb');

/**
 * Récupère les informations d'une personne en fonction de son identifiant unique.
 *
 * @param {string} personId - L'identifiant unique de la personne (nconst).
 * @returns {Promise<Object|null>} Une promesse qui se résout avec un objet contenant les informations de la personne.
 *                                La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.getPersonById = async (personId) => {
    try {
        return await personDb.getPersonById(personId);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Récupère les films associés à une personne (acteur, réalisateur, etc.) en fonction de son identifiant unique.
 *
 * @param {string} personId - L'identifiant unique de la personne (nconst).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les films associés à la personne.
 *                                  La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.getPersonMovies = async (personId) => {
    try {
        return await personDb.getPersonMovies(personId);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Recherche des personnes dans la base de données en fonction du nom ou du prénom via le module personDb.
 *
 * @param {string} searchedName - Le nom ou le prénom à rechercher.
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les personnes trouvées.
 *                                  Chaque objet contient le nom complet (concaténation du prénom et du nom) et l'identifiant unique de la personne (nconst).
 *                                  La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.searchPerson = async (searchedName) => {
    try {
        return await personDb.searchPerson(searchedName);
    } catch (error) {
        throw new Error(error);
    }
}

