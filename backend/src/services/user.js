const userDb = require('../models/userDb');
const bcrypt = require('bcryptjs');


/**
 * Enregistre un nouvel utilisateur dans la base de données.
 *
 * @param {string} username - Le nom d'utilisateur du nouvel utilisateur.
 * @param {string} lastname - Le nom de famille du nouvel utilisateur.
 * @param {string} firstname - Le prénom du nouvel utilisateur.
 * @param {string} birthdate - La date de naissance du nouvel utilisateur.
 * @param {string} password - Le mot de passe du nouvel utilisateur.
 * @returns {Promise<Object>} Une promesse qui se résout avec un objet contenant des informations sur l'opération d'inscription.
 *                           La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.signup = async (username, lastname, firstname, birthdate, password) => {

    try {
        const hash = await bcrypt.hash(password, 10);
        return await userDb.insertUser(username, lastname, firstname, hash, birthdate);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Gère le processus de connexion d'un utilisateur.
 *
 * @param {string} username - Le nom d'utilisateur de l'utilisateur tentant de se connecter.
 * @param {string} password - Le mot de passe fourni par l'utilisateur.
 * @returns {Promise<boolean>} Une promesse qui se résout avec un booléen indiquant si l'authentification a réussi.
 *                             La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.login = async (username, password) => {
    try {
        let userInfos = await userDb.getuserPasswordHash(username);
        if (userInfos != null) {
            const match = await bcrypt.compare(password, userInfos.mdp);
            if (match) {
                return true;
            } else {
                // Le mot de passe donné ne corresponds pas à celui dans la bdd
                return false;
            }
        }
        else {
            //pas d'utilisateur trouvé avec ce username
            return false;
        }
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Récupère les informations d'un utilisateur en fonction de son nom d'utilisateur.
 *
 * @param {string} username - Le nom d'utilisateur de l'utilisateur dont les informations sont récupérées.
 * @returns {Promise<Object|null>} Une promesse qui se résout avec un objet contenant les informations de l'utilisateur.
 *                                La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.getUserInformations = async (username) => {
    try {
        return await userDb.getuserInformations(username);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Récupère les films vus par un utilisateur en fonction de son nom d'utilisateur.
 *
 * @param {string} username - Le nom d'utilisateur de l'utilisateur dont les films vus sont récupérés.
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les films vus par l'utilisateur.
 *                                  Chaque objet du tableau a une structure similaire à celui renvoyé par userDb.getUserSeenMovies.
 *                                  La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.getUserSeenMovies = async (username) => {
    try {
        return await userDb.getUserSeenMovies(username);
    }
    catch (error) {
        throw new Error(error);
    }
}

/**
 * Modifie le mot de passe d'un utilisateur dans la base de données.
 *
 * @param {string} username - Le nom d'utilisateur de l'utilisateur dont le mot de passe doit être modifié.
 * @param {string} newPassword - Le nouveau mot de passe de l'utilisateur.
 * @returns {Promise<Object>} Une promesse qui se résout avec un objet contenant des informations sur l'opération de modification du mot de passe.
 *                           La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.changeUserPassword = async (username, newPassword) => {

    try {
        const hash = await bcrypt.hash(newPassword, 10);
        return await userDb.changeUserPassword(username,hash)
    } catch (error) {
        throw new Error(error);
    }
}