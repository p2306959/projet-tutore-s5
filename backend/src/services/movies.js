const moviesDb = require('../models/movieDb');

/**
 * Récupère les informations d'un film en fonction de son identifiant unique.
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @returns {Promise<Object|null>} Une promesse qui se résout avec un objet contenant les informations sur le film.
 *                                La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getMovieById = async (movieId) => {
    try {
        return await moviesDb.getMovieById(movieId);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Recherche des films en fonction du titre.
 *
 * @param {string} title - Le titre du film à rechercher.
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les films correspondant au titre.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.searchMovie = async (title) => {
    try {
        return await moviesDb.searchMovieByTitle(title);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Recherche des films dans la base de données en fonction des critères spécifiés via le module moviesDb.
 *
 * @param {string} title - Le titre à rechercher (peut être null ou undefined).
 * @param {number} dateMin - La date minimale de sortie des films (peut être null ou undefined).
 * @param {number} dateMax - La date maximale de sortie des films (peut être null ou undefined).
 * @param {string[]} genres - Un tableau d'identifiants de genres pour filtrer les films (peut être vide).
 * @param {string[]} personnes - Un tableau d'identifiants de personnes pour filtrer les films (peut être vide).
 * @param {string} order - Le champ selon lequel les résultats doivent être triés (peut être null ou undefined).
 * @param {boolean} croissant - Indique si les résultats doivent être triés par ordre croissant (true) ou décroissant (false).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les films filtrés.
 *                                  La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.searchMovieByCriteria = async (title, dateMin, dateMax, genres, personnes, order, croissant) => {
    try {
        return await moviesDb.searchMovie(title, dateMin, dateMax, genres, personnes, order, croissant);
    } catch (error) {
        throw new Error(error);
    }

};

/**
 * Récupère la distribution d'acteurs pour un film en fonction de son identifiant unique.
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur la distribution d'acteurs pour le film.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getMovieCast = async (movieId) => {
    try {
        return await moviesDb.getMovieCast(movieId);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Récupère l'auteur d'un film en fonction de son identifiant unique.
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur la distribution d'acteurs pour le film.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getMovieAuthor = async (movieId) => {
    try {
        return await moviesDb.getMovieAuthor(movieId);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Récupère la distribution d'acteurs pour un film en fonction de son identifiant unique.
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur la distribution d'acteurs pour le film.
 *                                  La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getMovieActors = async (movieId) => {
    try {
        return await moviesDb.getMovieActors(movieId);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Récupère la note attribuée par un utilisateur à un film en fonction des identifiants uniques du film et de l'utilisateur.
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @param {string} userId - L'identifiant unique de l'utilisateur (login).
 * @returns {Promise<Object|null>} Une promesse qui se résout avec un objet contenant la note attribuée par l'utilisateur au film.
 *                                La promesse se rejette avec une erreur si la requête échoue.
 */
exports.getMovieUserRate = async (movieId, userId) => {
    try {
        return await moviesDb.getMovieUserRate(movieId, userId);
    } catch (error) {
        throw new Error(error);
    }
}


/**
 * Insère ou met à jour la note attribuée par un utilisateur à un film en fonction des identifiants uniques du film et de l'utilisateur.
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @param {string} userId - L'identifiant unique de l'utilisateur (login).
 * @param {number} rate - La note attribuée par l'utilisateur au film.
 * @returns {Promise<Object>} Une promesse qui se résout avec un objet contenant des informations sur l'opération d'insertion ou de mise à jour.
 *                           La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.insertOrUpdateMovieUserRate = async (movieId, userId, rate) => {
    try {
        let existingRate = await moviesDb.getMovieUserRate(movieId, userId);
        if (existingRate == null) {
            //L'utilisateur n'a pas encore noté ce film, on ajoute la note
            return await moviesDb.insertMovieUserRate(movieId, userId, rate);
        } else {
            //L'utilisateur a déjà noté ce film, on modifie la note
            return await moviesDb.updateMovieUserRate(movieId, userId, rate);
        }
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Supprime la note attribuée par un utilisateur à un film en fonction des identifiants uniques du film et de l'utilisateur.
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @param {string} userId - L'identifiant unique de l'utilisateur (login).
 * @returns {Promise<Object>} Une promesse qui se résout avec un objet contenant des informations sur l'opération de suppression.
 *                           La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.deleteMovieUserRate = async (movieId, userId) => {
    try {
        return await moviesDb.deleteMovieUserRate(movieId, userId);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Récupère les films associés à un genre en fonction de son identifiant unique.
 *
 * @param {string} styleId - L'identifiant unique du genre (idGen).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les films associés au genre.
 *                                  La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.getMoviesByStyle = async (styleId) => {
    try {
        return await moviesDb.getMoviesByStyle(styleId);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Récupère les genres associés à un film en fonction de son identifiant unique.
 *
 * @param {string} movieId - L'identifiant unique du film (tconst).
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur les genres associés au film.
 *                                  La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.getMovieStyles = async (movieId) => {
    try {
        return await moviesDb.getMovieStyles(movieId);
    } catch (error) {
        throw new Error(error);
    }
}

/**
 * Récupère tous les genres depuis la base de données.
 *
 * @returns {Promise<Array<Object>>} Une promesse qui se résout avec un tableau d'objets contenant les informations sur tous les genres.
 *                                  La promesse se rejette avec une erreur si l'opération échoue.
 */
exports.getAllStyles = async () => {
    try {
        return await moviesDb.getAllStyles();
    } catch (error) {
        throw new Error(error);
    }
}

exports.getFilmsByAAS = async (personnes, genres, order, croissant, userId) => {
    try {
        return await moviesDb.getFilmsByAAS(personnes, genres, order, croissant, userId);
    } catch (error) {
        throw new Error(error);
    }
}