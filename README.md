# Projet tutoré S5

## Présentation

## Première installation

Installez Node.js avec le lien suivant :

https://nodejs.org/en

Copiez l'ensemble des fichiers avec la commande

```bash
git clone https://forge.univ-lyon1.fr/p2306959/projet-tutore-s5.git
```

### Pour le backend

Allez dans le dossier backend puis installez les dépendances nécéssaires au projet.

```bash
cd backend
npm install
```
### Pour la base de données

Afin de faire le lien avec la base de données, créez un fichier `.env` dans le dossier `backend` en reprenant la structure du fichier `.env.example` puis complétez-le.

Pour une base de données MySQL depuis XAMPP, les informations de connexion par défaut sont :

```ini
DB_HOST=localhost
DB_PORT=3306
DB_USER=root
DB_PASSWORD=""
DB_NAME=projet_S5
```

```DB_NAME``` doit correspondre au nom de la base de données que l'on va utiliser. 
On peut la créer dans phpMyAdmin (`Nouvelle base de données` dans le menu à gauche).

Créez les différentes tables nécéssaires dans la base de données. (Voir dans la branche `base_donnees` le fichier `src/sql/table.sql`)

### Pour le frontend

Allez dans le dossier backend puis installez les dépendances nécéssaires au projet.

```bash
cd ..
cd frontend
npm install
```


## Utilisation

- Démarrer la base de données (depuis XAMPP)
- Démarrer le backend depuis un terminal

```bash
node ./backend/src/server.js
```
- Pour démarrer le frontend Vue, ouvrez un nouveau terminal
```bash
cd frontend
npm run dev
```

On peut alors accéder au site à l'adresse qui est indiquée (`localhost:5173`)