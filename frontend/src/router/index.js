import { createRouter, createWebHistory } from 'vue-router'
import Accueil from '../views/Accueil.vue'
import Connexion from '../views/Connexion.vue'
import Inscription from '../views/Inscription.vue'
import MonCompte from '../views/MonCompte.vue'
import PageFilm from '../views/PageFilm.vue'
import SearchResults from '../views/SearchResults.vue'
import PagePersonne from '../views/PagePersonne.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      component: Accueil
    },
    {
      path: '/inscription',
      name: 'inscription',
      component: Inscription
    },
    {
      path: '/connexion',
      name: 'connexion',
      component: Connexion
    },
    {
      path: '/account',
      name: 'account',
      component: MonCompte
    },
    {
      path: '/movie/:id',
      name: 'movie',
      component: PageFilm
    },
    {
      path: '/search',
      name: 'search',
      component: SearchResults
    },
    {
      path: '/person/:id',
      name: 'person',
      component: PagePersonne
    },
  ]
})

export default router
